import axios from 'axios';
import { urlBase } from './store/constants';

const instance = axios.create({
    baseURL: urlBase
});

instance.defaults.baseURL = urlBase;
instance.defaults.headers.post['Content-Type'] = 'application/json'

export default instance;