import { ADD_BLITZ, ADD_BLITZS, API } from '../constants';
import axios from '../../axios';

export const addBlitzs = (blitzs) => ({
  type: ADD_BLITZS,
  payload: { blitzs }
});

const addBlitz = (blitz) => ({
  type: ADD_BLITZ,
  payload: { blitz }
});

// Examples using the custom api middlware
export const fetchFeed = () => ({
  type: API,
  url: '/api/feed',
  method: 'GET',
  success: addBlitzs,
});

export const createBlitz = (data) => ({
  type: API,
  url: '/api/blitzs',
  method: 'POST',
  data,
  success: addBlitz,
});


export const likeBlitz = (blitzId) => async (dispatch, getState) => {
  const config = {
    method: 'POST',
  };

  try {
    const res = await axios(`/api/blitzs/${blitzId}/like`, config);
    const blitz = res.data;
    dispatch(addBlitz(blitz));
  } catch (error) {
    console.log(error)
  }
}


