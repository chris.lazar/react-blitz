import { API, ADD_USER, ADD_USERS } from '../constants';
import { addBlitzs } from './blitz';
import { toggleFollow } from './currentUser';
import axios from '../../axios';

const addUser = (user) => ({
  type: ADD_USER,
  payload: { user }
});

const addUsers = (users) => ({
  type: ADD_USERS,
  payload: { users },
});

export const fetchUsers = () => ({
  type: API,
  url: '/api/users',
  success: addUsers
})

export const fetchUser = userId => async (dispatch, getState) => {
  try {
    const res = await axios(`/api/users/${userId}`);
    const user = res.data;
    const { blitzs } = user;
    dispatch(addBlitzs(blitzs))
    delete user.blitzs;
    dispatch(addUser(user));
  } catch (error) {
    console.log(error);
  }
}

export const toggleFollowUser = userId => async (dispatch, getState) => {
  const config = {
    method: 'POST',
  };
  try {
    const res = await axios(`/api/users/${userId}/follow`, config)
    const user = res.data;
    console.log('user: ', user);
    dispatch(addUser(user));
    dispatch(toggleFollow(user._id));
  } catch (error) {
    console.log('error: ', error);
  }
}
