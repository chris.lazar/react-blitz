import { API, REMOVE_CURRENT_USER, SET_CURRENT_USER, TOGGLE_FOLLOW } from '../constants';
import axios from '../../axios';

const setCurrentUser = (user) => ({
  type: SET_CURRENT_USER,
  payload: { user }
});

const removeCurrentUser = () => ({
  type: REMOVE_CURRENT_USER,
})

export const login = (data) => async (dispatch) => {
  const config = {
    method: 'POST',
    data: JSON.stringify(data),
  };
  try {
    const res = await axios(`/api/login`, config);
    const user = res.data;
    dispatch(setCurrentUser(user))
    localStorage.setItem('userToken', JSON.stringify(user.token));
    axios.defaults.headers.common = { 'Authorization': `Bearer ${user.token}` }
  } catch (error) {
    console.log(error);
  }
}

export const logout = () => (dispatch) => {
  dispatch(removeCurrentUser());
  localStorage.clear();
}

const registerUser = (user) => (dispatch) => {
  dispatch(setCurrentUser(user));
  localStorage.setItem('userToken', JSON.stringify(user.token));
}

export const register = (data) => ({
  type: API,
  url: '/api/users',
  method: 'POST',
  success: registerUser,
  data,
});


export const fetchCurrentUser = () => ({
  type: API,
  url: '/api/me',
  success: setCurrentUser,
});

export const fetchLocalUser = () => (dispatch) => {
  const userToken = JSON.parse(localStorage.getItem('userToken'));
  if (userToken) {
    axios.defaults.headers.common = { 'Authorization': `Bearer ${userToken}` }
    dispatch(setCurrentUser({ token: userToken }));
    dispatch(fetchCurrentUser());
  }
}

export const toggleFollow = (userId) => ({
  type: TOGGLE_FOLLOW,
  payload: { userId }
});
