import axios from '../axios';
import { API } from './constants';

export default ({ dispatch, getState }) => next => async action => {

  const { currentUser } = getState();
  if (currentUser.token) {
    axios.defaults.headers.common = { 'Authorization': `Bearer ${currentUser.token}` }
  }

  if (action.type !== API) {
    return next(action);
  }

  const config = {
    method: action.method || 'get',
  };

  if (action.data) {
    config.data = JSON.stringify(action.data);
  }

  const actionCreator = action.success;

  try {
    const result = await axios(`${action.url}`, config);
    const data = result.data;
    dispatch(actionCreator(data));

  } catch (error) {
    console.log('in da error');
    console.log(error);
  }
}